var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    var isActive = this.classList.contains("active");
    
    // Close all panels
    closeAllPanels();
    
    // Toggle the active class
    this.classList.toggle("active");
    
    // Open the panel if it was previously closed
    if (!isActive) {
      var panel = this.nextElementSibling;
      panel.style.display = "inline-block";
    }
  });
}

function closeAllPanels() {
  for (i = 0; i < acc.length; i++) {
    var panel = acc[i].nextElementSibling;
    panel.style.display = "none";
    acc[i].classList.remove("active");
  }
}